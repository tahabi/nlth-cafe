Docker runs via systemd:

[Unit]
Description=Docker container for nlth2
Requires=docker.service
After=docker.service

[Service]
Restart=on-failure
User=abizer
Group=www-data
WorkingDirectory=/home/abizer/projects/nlth2
ExecStart=/usr/bin/docker start -a a5c6eaf1cdc7
# bad idea to hardcode the container
ExecStop=/usr/bin/docker stop -t 2 a5c6eaf1cdc7

[Install]
WantedBy=multi-user.target