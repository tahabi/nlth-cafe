import sys, os
INTERP = os.path.join(os.environ['HOME'], 'cafe2.tahabi.com', 'bin', 'python')
if sys.executable != INTERP:
	os.execl(INTERP, INTERP, *sys.argv)
sys.path.append(os.getcwd())
from flaskr.flaskr import app as application

from werkzeug.debug import DebuggedApplication
application = DebuggedApplication(application, evalex=True)
