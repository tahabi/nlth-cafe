This is the Alsion No Left Turn Here Café system source code. The site is live at cafe2.tahabi.com.
 
There are two versions: the older one, which was written in PHP and based on a pseudo-MVC pattern (v2/) and the current one, written in Python, using Flask, and running on Passenger WSGI (v3/).

Please do not change /v3/wsgi_passenger.py.

Version 3 is currently feature-frozen, having reached the 3.0-complete development milestone. If you would like to work on it, please take the following steps:

1. Create a virtualenv

2. Install Flask (``` $ pip install Flask ```)

3. Clone the repo

4. ```$ . bin/activate ```

5. And use ```$ python nlth2.py ``` to start the development server, which binds to localhost:5000

Version 3.1 is undergoing testing. The only difference will be replacing Passenger with uWSGI and Dockerizing the project. 