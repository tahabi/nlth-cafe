<?php
error_reporting(E_ALL);

if ( isset($_GET['a']) )
{
	$action = $_GET['a'];
	$functions = get_defined_functions(); 

	if( in_array($action, $functions['user']) && substr($action,0,1) != "_" ) // get_defined_function['user'] prevents the bug with function_exists which allows arbitrary remote function execution
	{
	    /* taken from StackOverflow (http://stackoverflow.com/questions/6022231/simple-php-mvc-example)
			executes whatever is given by the action parameter, as long as it's a function 
			that exists and doesn't start with an underscore. Underscore limiter allows us to define
			private functions that can only be executed by the script and not the user. However, clever URL encoding could probably get around this restriction. 
		*/
		
	    try {
	    	
	    	$action(); 
	    }
	    
	    catch (Exception $e)
	    {
	    	error_log($e->getMessage()); 
	    	print $e->getMessage();
	    	debug_print_backtrace();	
	    }
	    
	}
	
	else
	{
		header("HTTP/1.1 501 Not Implemented");
		print "This particular call has not yet been implemented. Please check back later to see if it has."; 
	}

}
else if ( isset($_GET['b']) || isset($_GET['id']) )
{
	header("HTTP/1.1 400 Bad Request"); 
	print "Query string does not contain required parameters!"; 
}
else
{
	input(); // default to input form if no action is specified	
}

function input($x_name = NULL, $message = NULL)
{
	$model = _load_model('item', true); 

	_load_view( 'input' , array( "groups" => $model->item_array()) ); 
	// if x_name and message are null as they are by default, the isset() calls in the view will fail, causing no message to be displayed.
	
}

function statistics($optional_force_view = NULL)
{
	try {
		// _load_view('stats', array("model" => _load_model('stats', true))); 
		
		$model = _load_model('stats', true);

		if ( $optional_force_view != NULL)
		{
			$view = $optional_force_view; 
		}
		else if ( isset( $_GET['b'] ) )
		{
			$view = $_GET['b']; 
		}
		else
		{
			$view = 'stats_menu'; 
		}
		
		switch ( $view )
		{
			case 'all':
				$result = $model->getAll();
				break;
			case 'today':
				$result = $model->getToday(); 
				break;
			case 'yesterday':
				$result = $model->getYesterday();
				break;
			case 'date':
				if ( isset( $_GET['date'] ) )
					$result = $model->getDate( $_GET['date'] ); 
				else
					throw new Exception('Which date?');
				break;
			case 'sql':
				if ( isset( $_REQUEST['query'] ) ) 
				{
					$result = $model->resolveSQL( $_REQUEST['query'] ); 
				}
				else
				{
					_load_view('sql_form');
					return true;
				}
				break;
			case 'week':
				$result = $model->getWeek(); 
				break;
			default:
				_load_view('stats_menu');
				return true; 
				break;
		}
			$viewname = ( isset( $_GET['date'] ) ) ? $_GET['date'] : $view; 
			$id_map = $model->id_map; 
			_load_view('stats', array(
				
				'result' => $result, 
				'viewname' => $viewname, 
				'id_map' => $id_map,
			
			)); 
			// logic for $model->name here
			// load the id_map into a variable to pass to the view
			// display the view
			

		// print "got stats?";
	}
	catch ( Exception $e )
	{
		print $e->getMessage(); 
	}
}

function add()
{
	if ( isset($_GET['b']) )
	{
		if ( $_GET['b'] == 'item')
		{
			if ( isset($_POST['submit']) && $_POST['submit'] == 'Submit')
			{
				$model = _load_model('item', true); 
				$model->add($_POST['id'], $_POST['name'], $_POST['group'], $_POST['price'], $_POST['cost'], (isset($_POST['disabled']) ? $_POST['disabled'] : 0), $_POST['attr']);
				// header("Location: http://cafe.tahabi.com/modify/item/" . $_POST['id']); 
				_load_view('add_item', array('add_success' => true)); 
			}
			else 
			{
				_load_view('add_item'); // else no information has been POSTed, so show the relevant add item view
			}
		}
		elseif ( $_GET['b'] == 'transaction')
		{	
			if( isset($_POST['items']) ) 
			{
				_load_model('transaction', true)->add(); // handling done class-side to incorporate kitchen attribution
				// no need to reload the page because everything is done through jQuery's AJAX routines
			}
			else
			{
				input(); 
			}
		}	
		else 
		{
			throw new Exception("Unknown paramater: {$_GET['b']}"); 
			break;
		}
	}
	else
	{
		throw new Exception("Specification Required."); 	
	}
		
}

function delete()
{
	if (isset($_GET['b']))
	{
		if ( $_GET['b'] == 'item' && ( !isset($_GET['confirm']) || $_GET['confirm'] != 'yes' ) )
		{
			// I know this is bad practice in the controller script.
			print <<< END
			Are you sure you want to delete this item? Deleting items from the database, 
			especially items that have transactions associated with them, severely impacts 
			the system's ability to track what is and what isn't selling. This functionality
			should not be used at all unless you are very familiar with how the system works.
			If you've misspelled something, or want to hide an item from the main screen, 
			you can edit it <a href="http://cafe.tahabi.com/modify/item">here.</a>
			Use the "Disable item" setting to toggle hiding. If you are 100% sure you want to delete
			this item, click <a href="http://cafe.tahabi.com/controller.php?a=delete&b=item&id={$_GET['id']}&confirm=yes">here</a>.
END;
		}
		elseif ( $_GET['b'] == 'item' && $_GET['confirm'] == 'yes')
		{
			_load_model('item', true)->delete_item($_GET['id']);
			input("", "Item succcessfully deleted."); 
		}
		elseif ( $_GET['b'] == 'transaction' )
		{		
			_load_model('transaction', true)->delete_transaction($_GET['id']); 		
			header("Location: controller.php?a=statistics&b=" . $_GET['return']); 	
		}
		else
		{
			throw new Exception("Error: Specification Required.");
		}
	}
	else
	{
		throw new Exception("Specification Required."); 
	}	
}

function modify()
{
	if (isset($_GET['b']))
	{
		if ( $_GET['b'] == 'item')
		{
			if ( isset($_POST['ajax']) && $_POST['ajax'] == 'change' ) // A change was made from the AJAX
			{
				//error_log(serialize($_POST));
				$model = _load_model('item', true);
				$model->modify($_POST['id'], $_POST['name'], $_POST['group'], $_POST['price'], $_POST['cost'], $_POST['disabled'], $_POST['attr']); 
				echo json_encode(_load_model('item', true)->getItem($_POST['id']));
			}
			elseif ( !isset($_POST['ajax']) && isset($_POST['id']) ) // got a request from the old form-based POST item modify screen
			{
				$model = _load_model('item', true);
				$model->modify($_POST['id'], $_POST['name'], $_POST['group'], $_POST['price'], $_POST['cost'], $_POST['disabled'], $_POST['attr']);
				_load_view('item', array('change_success' => 'true', 'item' => _load_model('item', true)->getItem($_GET['id'])));
			}
			elseif ( isset($_POST['ajax']) && $_POST['ajax'] == 'get' && isset($_POST['id']) ) // if getting an AJAX request from the browser "all_items" page
			{
				echo json_encode(_load_model('item', true)->getItem($_POST['id']));
			}
			else // Show the item modification view if not through AJAX
			{
				if ( isset($_GET['id']) ) 
				{
					_load_view('item', array('item' => _load_model('item', true)->getItem($_GET['id'])));
				}
				else // if the url doesn't specify an ID, dsplay a list of items to choose from
				{
					$model = _load_model('item', true);
					_load_view('all_items', array('groups' => $model->item_array(true), 'items' => $model->get_all())); 
				}
			}
		}
		elseif ( $_GET['b'] == 'transaction' ) 
		{
			if ( isset($_POST['submit']) && $_POST['submit'] == 'Submit' ) // A change to a transaction was sent to the server
			{ 
				_find_stats_page($_REQUEST['return']); 
				$model = _load_model('transaction', true); 
				$model->modify($_GET['id'], $_POST['iid'], $_POST['name'], $_POST['date'], $_POST['price'], (isset($_POST['open']) ? $_POST['open'] : 'FALSE')); 
				statistics(); // _load_view('transaction', array("transaction" => $model->getTransaction($_GET['id']))); 
			}
			elseif ( isset($_GET['return']) && !isset($_GET['submit']) )
			{
				_load_view('transaction', array("transaction" => _load_model('transaction', true)->getTransaction($_GET['id']), "name2id" => _load_model('item', true)->getname2id(), "return" => $_GET['return']));
			}
			elseif ( isset($_GET['id']) ) // no change was sent, 
			{
				_load_view('transaction', array("transaction" => _load_model('transaction', true)->getTransaction($_GET['id']), 'name2id' => _load_model('item', true)->getname2id()));
				// _load_view('stats', array('model' => _load_model('stats', true, 'all'))); 
			}
			else {
				throw new Exception("Error: Need transaction ID.");
			}
		}		
		else 
		{
			throw new Exception("Unknown parameter: " . $_GET['b']); 
			break;
		}		
	}
	else
	{
		throw new Exception(__FUNCTION__ . " " . __LINE__ . ": second parameter not set."); 	
	}

}

function custom()
{
	if (isset($_GET['b']))
	{
		if ( $_GET['b'] == 'item')
		{
			_load_model('item', true)->add($_GET['id']); 
		}
		elseif ( $_GET['b'] == 'transaction')
		{
			if ( isset($_POST['submit']) && $_POST['submit'] == 'CustomFormSubmit') 
			{
				_load_model('transaction', true)->custom_add($_POST['id'], $_POST['name'], $_POST['date'], $_POST['period']);
				_load_view('custom_add_transaction', array(
				'id' => $_POST['id'], 
				'name' => $_POST['name'], 
				'date' => $_POST['date'], 
				'period' => $_POST['period'], 
				'price' => $_POST['price'],
				'name2id' => _load_model('item', true)->getname2id(), 
				'message' => "{$_POST['name']}'s order of {$_POST['id']} is complete."));  		
				// header("Location: controller.php?a=stats&b=" . $_GET['return']); 
			}
			else
			{
				_load_view('custom_add_transaction', array('id' => '', 'name' => '', $date => '', $period => '', 'name2id' => _load_model('item', true)->getname2id())); 	
			}
		}
		else 
		{
			throw new Exception("Error: Specification Required.");
		}
	}
	else
	{
		throw new Exception("Specification Required."); 
	}	
}

function testing()
{
	if ( isset( $_GET['b'] ) )
	{
		switch ( $_GET['b'] ) 
		{
			case 'order_online':
				/*if ( isset($_POST['submit']) )
				{
					$model = _load_model('transaction', true); 
					$model->remote_order_form_add($_POST['name'], $item); 
					
					
					_load_view('online_order', array( "groups" => _load_model('item', true)->item_array(), "message" => "Thank you for your order! If you would like to order something else, please do so. Otherwise, you can close this window, we have received your order." ) );
				}
				else
				{
					_load_view('online_order', array( "groups" => _load_model('item', true)->item_array() ) );	
				}*/
				$model = _load_model('db', true);
				var_dump( $model->retrieve_data($model->make_query("SELECT name FROM items WHERE `group` = '!Popular'", __LINE__, __FUNCTION__, __CLASS__)) ); 
				break;
			case 'button_view':
				if ( isset($_POST['bvsubmit']) )
				{
					$model = _load_model('transaction', true); 
					foreach( $_POST['selected_items'] as $item )
					{
						$model->optimized_add($_POST['name'], $item); 
					}
					
					_load_view('button_view', array( "groups" => _load_model('item', true)->item_array() ) );
				}
				else
				{
					_load_view('button_view', array( "groups" => _load_model('item', true)->item_array() ) ); 	
				}
				break;
			case 'dump':
				var_dump($_REQUEST); 	
				break;
			case 'phpinfo':
				phpinfo(); 
				break;
			case '404':
				print "It seems the page you requested cannot be found. Please try another page.";
				break;
			case 'modifyitem':
				
				break;
			default:
				print "Unrecognized parameter {$_GET['b']}"; 
				break;
		}
	}	
}

function direct_interface() // a = 'direct_interface', b is the auth key
{
	if ( isset( $_REQUEST['b'] ) && $_REQUEST['b'] == "poiuytrewq" )
	{
		if ( isset( $_REQUEST['query'] ) )
		{
			$db = _load_model('db', true);
			$result = $db->make_query($_REQUEST['query'], __LINE__, __FUNCTION__, __CLASS__);

			if ($db->error)
			{
				die($db->errno . ' ' . $db->error . ': ' . $db->ex_message); 
			}

			else {	
				while ( $x = $result->fetch_assoc() ) 
				{
					echo json_encode($x) . '\n';
				}
			}
		}
		else
		{
			throw new Exception("Error: no query provided.");
		}
	}
	else
	{
		header("HTTP/1.1 401 Not Authorized");
	}
}

function _load_view($view_name, $data = NULL)
{
	if ( isset($data) )
	{
		if ( is_array($data) )
		{
			foreach( $data as $key => $value )
			{
				$$key = $value; // $$ makes the variable's value into another variable's name, allowing variable creation out of an associative array
			}
		}
	}
		
	require('views/'.$view_name.'.php');  	
}

function _load_model($name, $returnItem = false, $argument = false)
{ 
	require_once('models/'.$name.'.php');
	
	if ($returnItem)
	{
		$name = ucfirst($name);
		
		if ( $argument )
			return new $name($argument);
		else
			return new $name(); 
	}
	
}

function _find_stats_page($reqval)
{
	if ( $reqval == 'all' || $reqval == 'today' ) 
	{
		$_GET['b'] = $reqval; 	
	}
	else if ( preg_match('/[0-9]-[0-9]-[0-9]/', $reqval) )
	{	
		$_GET['b'] = 'date'; 
		$_GET['date'] = $reqval; 
	}
	else
	{
		return false; 	
	}  
}

?>