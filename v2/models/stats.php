<?php

require_once('db.php');


class Stats extends DB
{

	public $id_map; 
	public $result;
	public $error;
	public $action;
	public $name;
	public $fieldcount; 
	
	public function __construct($view = false)
	{
		parent::__construct(); 
		// self::resolve_request($view); 
		
		$result = self::make_query('SELECT * FROM items', __LINE__, __FUNCTION__, __CLASS__); 
		
		$data = array(); 
		
		while($item = $result->fetch_assoc())
		{
			$this->id_map[$item['id']] = $item; 	
		}
		
		//$this->name = isset( $_GET['date'] ) ? $_GET['date'] : $_GET['q'];
		
		$this->fieldcount = parent::getFieldCount();
	}

	private function resolve_request( $view = false )
	{
		if ( !isset($_GET['q']) && $view ) 
		{
			$this->action = "menu";
			$this->error = "No Parameters.";
		}
		elseif ( $view )
		{
			$this->result = self::queryer($view); 
		}
		else {
			$limit = isset($_GET['limit']) ? $_GET['limit'] : 1000;
			
			 
			
				//$_GET['q'] . ( (isset($_GET['date']) || isset($_REQEST['sql']) ) ? (isset($_GET['date']) ? "&date=" . $_GET['date'] : "&sql=" . urlencode($_REQUEST['sql'])) : "");
				// this little bit helps return the page to where we were after a delete. 
				// the first bit is the action, the main separator 
				// the second bit adds the date and sql, if they are there - if they exist, it appends it, else it appends nothing. 
				// when appending "it" it checks whether sql or date is called, and then appends the one that was with some nested ternerary operators
				
				$this->result = self::queryer($_GET['b'], $limit); 
			}
	}
	
	private function queryer($view, $limit = 1000)
	{
		switch ( $view )
		{
			case "all":
				return self::getAll(); 
			    break; 
			case "today":
				return self::getToday(); 
				break;
		 	case "yesterday":
				return self::getYesterday(); 
				break;
			case "date":
				return self::getDate($_GET['date']); 
				break;
			case "dateselect":
				$this->action = 'date'; 
				break;
			case "sql":
		  		if (isset($_REQUEST['sql']))
					return self::resolveSQL($_REQUEST['sql']);
				else  	  	  
					$this->action = 'sql';
				break;
		  	default:
		  		header('HTTP/1.1 400 Bad Request');
				throw new Exception('Unrecognized Parameter!'); 	
				break;
		}
	}
	
	function getAll()
	{
		 return self::make_query("SELECT * FROM `transactions` ORDER BY `id` DESC", __LINE__, __FUNCTION__, __CLASS__); 
	}
	
	function getToday()
	{
		return self::make_query("SELECT * FROM `transactions` WHERE `date` = CURDATE() ORDER BY `id` DESC LIMIT 0, 1000", __LINE__, __FUNCTION__, __CLASS__); 
	}
	
	function getYesterday()
	{
		$yesterday = date('Y-m-d', strtotime('yesterday ' . date('Y-m-d'))); # a hack method, I know. 
		return self::make_query("SELECT * FROM `transactions` WHERE `date` = '{$yesterday}' ORDER BY `id` DESC LIMIT 0, 1000", __LINE__, __FUNCTION__, __CLASS__); 
	}
	
	function getDate($date)
	{
		return self::make_query("SELECT * FROM `transactions` WHERE `date` = '{$date}' ORDER BY `id` DESC LIMIT 0, 1000", __LINE__, __FUNCTION__, __CLASS__);
	}

	function getWeek($date)
	{

	}
	
	function resolveSQL($sql)
	{
		return self::make_query($sql, __LINE__, __FUNCTION__, __CLASS__);
	}
}

?>