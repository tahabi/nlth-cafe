var names = 
["Abhinav Arora",
"Abizer Lokhandwala",
"Alexandra Petersen",
"Alhad Deshpande",
"Ali Ashraf",
"Allen Wang",
"Alyssa Vessel",
"Amalia Barrett",
"Ananth Ashok",
"Ananya Hindocha",
"Andrei Volgin",
"Anika Varma",
"Ashley Lam",
"Ben Rosete",
"Blake Werner",
"Brittany Lau",
"Carly Yang",
"Darren Tong",
"Devin Bog",
"Faith Rovetta",
"Ganna Malik",
"Grace Hsiang",
"Harmani Sethi",
"Isaac Daviet",
"Jamie Lam",
"Jessamyn Fathi",
"Jessica Anthony",
"Jessica Oak",
"Kabir Sawal",
"Kai Craig",
"Krti Tallam",
"Lindsay Kleytman",
"Mark Emmons",
"Meghna Agarwal",
"Michiko Zerda",
"Nathan Cooper",
"Neda Farr",
"Nidhi Gogri",
"Nikhila Rao",
"Noor Tai",
"Pavan Gowda",
"Rhea Kerawala",
"Rishab Goel",
"Rijul Singhal",
"Rohit Vinjamuri",
"Samantha Mejia",
"Sarah Buzsaki",
"Sharon Yin",
"Sophia Wagganer",
"Stevi Ibonie",
"Tanvi Kaur",
"Timothy Yang",
"Tyler Hertel",
"Vivek Hatte",
"Vrinda Suresh",
"Young Wang",
"Taher Lokhandwala",
"Justin Tong",
"Rishabh Singhal",
"Abigail Kelley", 
"Mrs. Mimi", 
"Mr. Bill", 
"Ms. Jennifer", 
"Ms. Michelle",
"Mrs. Buzsaki",
"Mrs. Petersen",
].sort();
		
function checkname() {
//	alert("Hello"); // + $("input:text").attr('value'));
	
	if( jQuery.inArray( document.getElementById("autoname").value, names) > -1 )
	{	
		return true; 
	}
	else 
	{
		alert("No student by that name attends Alsion.");
		return false;
	}
}

function deletetransaction(tid, caller)
{
	window.location = "http://cafe.tahabi.com/controller.php?a=delete&b=transaction&id=" + tid.toString() + "&return=" + caller; 
}	

function modifytransaction(tid, caller)
{
	window.location = "http://cafe.tahabi.com/controller.php?a=modify&b=transaction&id=" + tid.toString() + "&return=" + caller; 
}

function follow(obj)
{
		window.location = "http://cafe.tahabi.com/modify/item/" + obj.value 
}

x = [];

function jquery_update()
{
	x = [];
	cost = 0.0; 
	$('.box:checked').each( function(index, element)
	{
		m = $(element);
		for ( i = 0; i < m.parent().siblings().find('.quantity').val(); i++)
		{
			x.push(m.attr('data-id'));
			cost += parseFloat(m.attr('data-cost')); 
		}
	});

	$('#total_cost').text("$" + cost.toFixed(2));
	$('#change_5').text("$" + (5 - cost.toFixed(2)).toFixed(2));
	$('#change_10').text("$" + (10 - cost.toFixed(2)).toFixed(2));
	$('#change_20').text("$" + (20 - cost.toFixed(2)).toFixed(2));
}

function post_data()
{
	$.post('http://cafe.tahabi.com/add/transaction', 
		{ 
			'items[]': x, 
			'name': $('#autoname').val() 

		}).done(function(data) 
		{ 
			update_transaction_list();
			$('#customer_name').text($('#autoname').val() + "'s order has been processed.");
			$('#alert_response_field').text( data ); 
			$('.box:checked').change();
			$('#autoname, .box').val('').removeAttr('checked');
			jquery_update();
		});
}

function redirect_stat_dates()
{
	date = $('input:text').val();
	
	window.location = "http://cafe.tahabi.com/statistics/date/" + date; 
}

$( function() {
	$('.box').change(
		function() {
		    $(this).parent().siblings().toggleClass('disabled'); 
			jquery_update(); 
		}
	);

	$('.quantity').change(
		function() {
			jquery_update(); 
		}
	);

	$( ".submit_button").button().click( function()
	{
		post_data();
	});

	$("#second").button(); 
	$("#date").datepicker({ dateFormat: "yy-mm-dd" });

	$("input:text").autocomplete({
			source: function( request, response ) {
            var matcher = new RegExp( "^" + $.ui.autocomplete.escapeRegex( request.term ), "i" );
            response( $.grep( names, function( item ){
                return matcher.test( item );
            }) );
        },
			delay: 0,
			autoFocus: true
		});

});

function newfollow(id)
{
	$('._normal').removeClass("_pending _success").html("Please click on the name of the item you want to edit, <br /> make your changes above and click \"submit\" to save").hide().fadeToggle(); 
	item = {}; 
	$.post('http://cafe.tahabi.com/modify/item', 
		{
			'id': id,
			'ajax': 'get'
		}, 
		function(data) 
		{ 
			item = data;
			$('.item_name').text(item['name']).val(item['name']);
			$('.item_id').text(item['id']).val(item['id']); 
			$('.item_group').val(item['group']); 
			$('.item_price').val(item['price']); 
			$('.item_cost').val(item['cost']);
			$('.item_attr').val(item['attr']); 

			if ( item['disabled'] == "1" )
			{
				$('.item_disabled_false').prop('checked', '');
				$('.item_disabled_true').prop('checked', 'checked');
			}
			else
			{
				$('.item_disabled_true').prop('checked', '');
				$('.item_disabled_false').attr('checked', 'checked');
			}
		}, 
		'json');
}

function item_change_submit()
{
	$('._normal').addClass("_pending").text("Saving changes..."); 
	$.post('http://cafe.tahabi.com/modify/item',
	{
		ajax: 'change', 
		id: $('.item_id').val(), 
		name: $('.item_name').val(), 
		group: $('.item_group').val(), 
		price: $('.item_price').val(), 
		cost: $('.item_cost').val(), 
		attr: $('.item_attr').val(), 
		disabled: $('input[name=disabled]:checked').val()
	}, 
	function(data) 
	{
		console.log(data);
		$('._normal').toggleClass('_pending _success').delay ( 800 ).text("Changes made successfully!").delay( 1000 ).fadeToggle( 1500 ); 
		
		item = data;
		$('.item_name').text(item['name']).val(item['name']);
		$('.item_id').text(item['id']).val(item['id']); 
		$('.item_group').val(item['group']); 
		$('.item_price').val(item['price']); 
		$('.item_cost').val(item['cost']);
		$('.item_attr').val(item['attr']); 

		if ( item['disabled'] == "1" )
		{
			$('.item_disabled_false').prop('checked', '');
			$('.item_disabled_true').prop('checked', 'checked');
		}
		else
		{
			$('.item_disabled_true').prop('checked', '');
			$('.item_disabled_false').attr('checked', 'checked');
		}
	},
	'json');
}

function update_transaction_list()
{
	var transaction_container = $('<div class="transaction_container"></div>');
	transaction_container.append('<p>' + $('#autoname').val() + ':</p>');
	transaction_container.append($('<ul> </ul>'));
	for( var i = 0; i < x.length; i++ )
	{
		transaction_container.children('ul').append('<li>' + $('*').find("[data-id=" + x[i] + "]").next().text() + '</li>'); 
	}
	$('#transaction_list').append(transaction_container);
}
