<?php

require_once('models/db.php'); 

class Item extends DB {
	
	public function __construct() { parent::__construct(); }
	
	public function add($id, $name, $group, $price, $cost, $disabled, $attr) 
	{
		$id = md5($id);
		$disabled = ( $disabled == 'TRUE' ) ? 'TRUE' : 'FALSE';
		
$sql = <<< SQL
		
INSERT INTO `items` (`id`, `name`, `group`, `price`, `cost`, `disabled`, `attr`)
VALUES ('{$id}', '{$name}', '{$group}', '{$price}', '{$cost}', '{$disabled}', '{$attr}')
		
SQL;
	
	return self::make_query($sql, __LINE__, __FUNCTION__, __CLASS__); 

	}

	public function get($id, $what) {}
	
	public function getItem($id) 
	{
		return self::make_query("SELECT * FROM `items` WHERE `id` = '{$id}'", __LINE__, __FUNCTION__, __CLASS__)->fetch_assoc(); 
	}
	
	public function getname2id()
	{
		return self::make_query("SELECT name, id FROM `items`", __LINE__, __FUNCTION__, __CLASS__); 	
	}
	
	public function get_all() 
	{
		return self::make_query("SELECT * FROM `items`", __LINE__, __FUNCTION__, __CLASS__); 	
	}
	
	public function item_array($modify_view = false) 
	{
		$items = self::get_all();
		
		$groups = array(); 
		
		while ($row = $items->fetch_assoc())
		/* iterate through every item returned. */
		{
			if ( $modify_view == true ) // ( for use in the all_items view )
			{
				$groups[trim($row['group'])][] = $row;
			}
			else {
				if ($row['disabled'] != '1') // this hides the group name from showing if there are no items within the group that are enabled (in the main input view)
					$groups[trim($row['group'])][] = $row;
			}
				
		}
		
		ksort($groups, SORT_STRING);
		
		return $groups;
	
	}
	
	public function delete_item($id)
	{
		$sql = <<< END
		
		DELETE FROM items
		WHERE id = '{$id}'
END;

	return self::make_query($sql, __LINE__, __FUNCTION__, __CLASS__); 
	
	}
	
	public function modify($id, $name, $group, $price, $cost, $disabled, $attr)
	{
		
		$disabled = ( $disabled ) ? 'TRUE' : 'FALSE';  
		
		$sql = <<< SQL
		
		UPDATE items
		SET `name` = '{$name}',
			`group` = '{$group}', 
		    `price` = '{$price}',
		    `cost` = '{$cost}',
		    `disabled` = {$disabled},
		    `attr` = '{$attr}'
		WHERE `id` = '{$id}'		
SQL;

	self::make_query($sql, __LINE__, __FUNCTION__, __CLASS__, 'This query seems to be unreliable, in item->modify'); 
	
	}
	
	function get_by_attr($attr)
	{
		$sql = " SELECT `id` FROM `items` WHERE `attr` = '{$attr}'";
		$x = self::make_query($sql, __LINE__, __FUNCTION__, __CLASS__);
		$ret = array();
		while ( $y = $x->fetch_assoc() )
		{
			$ret[$y['id']] = true; 
		}
		return $ret; 
	}
	
	
}

?>