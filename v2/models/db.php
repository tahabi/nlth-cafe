<?php

class DB {
	
	private $mysqli;
	private $thread_id;
	
	private $dbhost = 'mysql.tahabi.com'; 
	private $dbusername = "nlth2usr"; 
	private $dbpassword = "nlth2pwd"; 
	private $dbname = "nlth2";

	public $errno;
	public $error;
	public $ex_message; 
	
	public function __construct()
	{
		$this->mysqli = new MySQLi($this->dbhost, $this->dbusername, $this->dbpassword, $this->dbname);
		/* opens the connection to the database */
		
		$this->mysqli->query("SET time_zone = '-8:00'");
		/* to fix the timezone problem with the statistics views */
		
		$this->thread_id = $this->mysqli->thread_id; 
		/* save the thread id for use in the __destruct function. */
		
	}

	/* A debug version of the call, to prevent modifying the database
	public function make_query($query, $line, $function, $class, $exception_message)
	{
		print $function . ": " . $query;
	}
	*/
	
	public function make_query($query, $line, $function, $class, $exception_message = false)
	{ 
		$result = $this->mysqli->query($query);

		$this->errno = $this->mysqli->errno; 
		$this->error = $this->mysqli->error; 
		$this->ex_message = $exception_message; 
		
		if( $this->mysqli->errno == 0 ) // if the query succeeded
		{
			// $this->errlog($class, $function, $line, $this->mysqli->errno, $this->mysqli->error, $query, $exception_message); 
			// uncomment upper line to log every query, else only log failed queries
			
			return $result; 
		} 
		else // the query failed.
		{
			$exception_message = (!$exception_message) ? $this->mysqli->error : $exception_message;
			// if $exception message is false, give it a real message.
				
			$this->errlog($class, $function, $line, $this->mysqli->errno, $this->mysqli->error, $query, $exception_message);
			// log the class, function, and line that caused the problem, as well as the Mysql error number, error, and the sql query that caused it.

			throw new Exception($exception_message);	
			// Throw an exception, causing the execution chain to break immediately. This should be the lowest level Exception that can be thrown.
			
			return false;
			// A backup. Should never be executed. 
		}
		
	}
	

	
	public function make_multi_query($query_array, $line, $function, $class, $exception_message = false)
	{		// query array allows for better error checking. 
		if ($this->mysqli->multi_query(implode(';', $query_array))) // statements must be joined with semicolons
		{
			$results = array(); 
			$i = 0; // runs through the result statements, allows us to catch erroneous queries (from the PHP docs for mysqli->multi_query comments)
			do {
				if ($result = $this->mysqli->store_result())
				{
					$results[] = $result->fetch_assoc(); 
					$result->free();
					$i++;
				}
			} 
			while ($this->mysqli->more_results() && $this->mysqli->next_result());
			
			if ( $this->mysqli->errno ) 
			{
				$exception_message = (!$exception_message) ? $this->mysqli->error : $exception_message;
				$query_failure = "Query failed on query " . $i . ": " . $query_array[$i]; 
				$this->errlog($class, $function, $line, $this->mysqli->errno, $this->mysqli->error, implode(';\n', $query_array), $query_failure);
				throw new Exception($query_failure); 
				return false;
			}
			else
			{
				$this->errlog($class, $function, $line, $this->mysqli->errno, $this->mysqli->error, implode(';\n', $query_array), "No error recorded in query."); 
				return $results; 	
			}
			
		}
		else
		{
			$this->errlog($class, $function, $line, $this->mysqli->errno, $this->mysqli->error, implode(';\n', $query_array), "Entire query failed to execute.");  	
		}
	}
		
	public function errlog($class, $function, $line, $errno, $error, $query="", $exception_message = NULL)
	{
		$open = fopen("nlth.log", "a+");
		/* opens the errorlog file. */
		
		$date = date("m/d/y g:i:s a"); 
		/* sets the date in the format 10/31/12 10:45:43 am */
		
		$text = <<< TEXT
Error: ({$date}) [{$errno}] {$error}
Message: {$exception_message}
Class: {$class} Function: {$function} Line: [{$line}]:
Query: {$query}
-\n
TEXT;

		fwrite($open, $text); 
		fclose($open);
		/* writes to the file and closes it. Performance impact unknown. */
	}
	
	public function getFieldCount()
	{
		return $this->mysqli->field_count; 	
	}
	
	public function retrieve_data($result)
	{
		$res = array(); 
		while ( $x = $result->fetch_assoc() )
		{
			foreach( $x as $v )
			{
				array_push($res, $v);	
			}
		}
		return $res; 
	}
	
	public function __destruct()
	{ 
	//	return true; 
		
		$this->mysqli->kill($this->thread_id); 
		$this->mysqli->close();
		/* written to solve problems with too many threads being left open by the server during normal cafe usage. 
			performance impact unknown. */
			
		return true;
	}
	
}



?>