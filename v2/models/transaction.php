<?php 

require_once('models/db.php');
require_once('models/item.php'); 

class Transaction extends DB
{
	public function __construct() 
	{
		parent::__construct();
	}
	
	public function add() // still handled class-side to implement cooking
	{
		$items = $_POST['items'];
		
		$item = new Item(); 
		$kitchen = $item->get_by_attr('kitchen');
		
		foreach ($items as $k3)
		{
			$cooked = (array_key_exists($k3, $kitchen)) ? 'TRUE' : 'FALSE';
			self::optimized_add($_POST['name'], $k3, $cooked); 
		}
	}
	
	public function optimized_add($name, $item, $cooked)
	{
		$x = <<< SQL
		 INSERT INTO `transactions` (`id`, `name`, `iid`, `date`, `price`, `open`) 
		 VALUES (NULL, '{$name}', '{$item}', CURDATE(), (SELECT `price` FROM `items` WHERE `id` = '{$item}'), '{$cooked}')
SQL;
		self::make_query($x, __LINE__, __FUNCTION__, __CLASS__, "Check log for problematic query.");
	}
	
	public function custom_add($id, $name, $date, $price)
	{
		$x = "INSERT INTO `transactions` (`id`, `name`, `iid`, `date`, `price`) VALUES (NULL, '{$name}', '{$item}', '{$date}', '{$price}')";
		self::make_query($x, __LINE__, __FUNCTION__, __CLASS__);
	}
	
	public function delete_transaction($id)
	{
		$sql = <<< END
		
		DELETE FROM transactions
		WHERE id = '{$id}'
END;
		self::make_query($sql, __LINE__, __FUNCTION__, __CLASS__); 
	}
	
	public function modify($id, $iid, $name, $date, $price, $open)
	{
		$sql = <<< END
		UPDATE `transactions` 
		SET `iid` = '{$iid}', 
			`name` = '{$name}', 
			`date` = '{$date}',
			`price` = '{$price}',
			`open` = '{$open}'
		WHERE `id` = '{$id}'
END;
		self::make_query($sql, __LINE__, __FUNCTION__, __CLASS__); 
	}
	
	public function getTransaction($id)
	{	
		return self::make_query("SELECT * FROM `transactions` WHERE `id` = '{$id}'", __LINE__, __FUNCTION__, __CLASS__)->fetch_assoc(); 
	}
	
	public function remote_order_form_add($name, $item)
	// this needs to be fixed
	{
		$items = new Item();
		$cooked = $items->get_by_attr('kitchen');
		
		$items = array(); 
		$period = (date("H") < 16) ? "1" : "2"; 
		
		foreach ($_POST as $key => $val) 
		{ 
			if ($val[0] != "true" || $key == "name") 
			{
				continue;
			}
			
			if (isset($val[1]))
			{
				for ($i = 0; $i < $val[1]; $i++)
				{
					array_push($items, $key);
				}
			}
			else 
			{
				array_push($items, $key);
			}  
		}
		
		foreach ($items as $k3)
		{	
			if ( isset($cooked['attr'][$k3]) ) 
			{
				$transactionsql = "INSERT INTO `transactions` (`id`, `name`, `iid`, `date`,`open`, `cost`) VALUES ('{$k3}', '". $_POST['name'] ."', NULL, CURDATE(), '{$period}', 1, (SELECT `cost` FROM `items` WHERE `id` = '{$k3}'))";
			}
			else
			{
					$transactionsql = "INSERT INTO `transactions` (`id`, `name`, `tid`, `date`, `period`, `cost`) VALUES ( '{$k3}', '". $_POST['name'] ."', NULL, CURDATE(), '{$period}'), (SELECT `cost` FROM `items` WHERE `id` = '{$k3}'))";
			}
			
			self::make_query($transactionsql, __LINE__, __FUNCTION__, __CLASS__); // inherited from base class
		
			$transactionsql = <<<SQL
			
			UPDATE `items`
			SET `inv` = (SELECT `inv`) - 1
			WHERE `id` = '{$k3}'
			LIMIT 1
			
SQL;
			self::make_query($transactionsql, __LINE__, __FUNCTION__, __CLASS__);  
		}
	}
}

?>