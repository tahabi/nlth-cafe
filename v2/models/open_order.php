<?php

require_once('transaction.php');

// This class will handle everything related to open orders.

class OpenOrders extends Transaction
{
	private $cooked_items; 
	
	public function __construct() 
	{
		parent::__construct();
		$this->cooked_items = self::retrieve_data(self::make_query("SELECT id FROM items WHERE `group` = '!Popular'", __LINE__, __FUNCTION__, __CLASS__)); 
	}
	
	public function new_order()
	{
		$items = array(); 
		foreach ($_POST as $key => $val) 
		{ 
			if ($val[0] != "true" || $key == "name") 
			{
				continue;
			}
			
			if (isset($val[1]))
			{
				for ($i = 0; $i < $val[1]; $i++)
				{
					array_push($items, $key);
				}
			}
			else 
			{
				array_push($items, $key);
			}  
		}
		
		foreach( $items as $item )
		{
			if ( isset( $this->cooked_items[$item] ) )
			{
				$y = "INSERT INTO `open_orders` (`tid`, `id`, `name`, `date`, `open`) VALUES (NULL, '{$item}', '{$name}', '{$date}', 1)";
				self::make_query($y, __LINE__, __FUNCTION__, __CLASS__, ""); 
			}
			
			self::optimized_add($_POST['name'], $item);
		}
	}
	
	public function close_order($tid)
	{
		$x = <<< SQL
		
		UPDATE `open_orders`
		SET `open` = 0
		WHERE `tid` = '{$tid}'
SQL;

		self::make_query($x, __LINE__, __FUNCTION__, __CLASS__, ""); 
	}
	
	public function get_order($tid)
	{
		$x = <<< SQL
		SELECT * FROM `open_orders`
		WHERE `tid` = '{$tid}'
SQL;

		self::make_query($x, __LINE__, __FUNCTION__, __CLASS__, ""); 
	}
	
}

?>