<html>
	<head>
    	<title><?php echo $item['name']; ?></title>
    </head>
    <style>
		table, h1 {margin-right:auto;margin-left:auto;text-align:center;}
		input[type=text] {width:200px;}
		td {border-bottom:1px solid black;}
	</style>
<body>
    <?php include('views/navigation.html'); ?>
<h1> Editing item: <?php echo $item['name'] . ' (' . $item['id'] . ')'; ?> </h1>
<?php if ( $change_success == 'true' ) { ?>
    <h2> Item successfully modified. </h2>
<?php } ?>
<table>
	<form method="post" action="http://cafe.tahabi.com/controller.php?a=modify&b=item&id=<?php echo $item['id']; ?>">
	<tr>
    	<td>Group</td>
        <td> <input type="text" name="group" value="<?php echo $item['group']; ?>" /> </td>
    </tr>
	<tr>
		<td>Name</td>
        <td> <input type="text" name="name" value="<?php echo $item['name']; ?>" /> </td>
    </tr>
    <tr>
    	<td>ID Seed (initials of the group + name)</td>
        <td> <input type="text" name="id" value="<?php echo $item['id']; ?>" /> </td>
    </tr>
    <tr>
    	<td>Price</td>
        <td> <input type="text" name="price" value="<?php echo $item['price']; ?>" /> </td>
    </tr>
    <tr>
    	<td>Cost to us</td>
        <td> <input type="text" name="cost" value="<?php echo $item['cost']; ?>" /> </td>
    </tr>
    <tr>
        <td>Attributes</td>
        <td> <input type="text" name="attr" value="<?php echo $item['attr']; ?>" /> </td>
    <tr>
    	<td>Disabled Item?</td>
        <td> Yes: <input type="radio" name="disabled" value='1' <?php echo ($item['disabled'] == '1') ? 'checked' : ''; ?> /> No: <input type="radio" name="disabled" value='0' <?php echo ($item['disabled'] == '0') ? 'checked' : ''; ?> /></td>
    </tr>
    <tr>
    	<td>Submit</td>
        <td> <input type="submit" name="submit" /> </td>
    </tr>
    </form>
</table>
</body>
</html> 