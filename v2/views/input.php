<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
   		<title>No Left Turn Here Cafe MVC v2.0</title>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
			<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
			<link type="text/css" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/smoothness/jquery-ui.css"  rel="stylesheet" />
			<script src="functionality.js" type="text/javascript"></script>
			<link href="http://cafe.tahabi.com/style.css" rel="stylesheet" type="text/css" />
	</head>

	<body>
		<?php include('views/navigation.html'); ?>
    	<div class="container">
          <div class="header"><a href="http://cafe.tahabi.com/"><img src="http://cafe.tahabi.com/logo.png" alt="NLTH" width="300" height="120" id="Insert_logo" /></a> 
            <!-- end .header -->
          </div>
          <div class="sidebar1" style="position:fixed;">
            <ul class="nav">
              <li><a href="http://cafe.tahabi.com/add/item">Add new item</a></li>
              <li><a href="http://cafe.tahabi.com/modify/item">Update inventory</a></li>
			  <li><a href="http://cafe.tahabi.com/statistics/">View stats</a></li>
			  <li><a href="http://cafe.tahabi.com/statistics/today">Today's Sales</a></li>
            </ul>
              
                <p> Change from a 5: <span id="change_5"> $0.00 </span></p>
                <p> Change from a 10: <span id="change_10"> $0.00 </span></p>
                <p> Change from a 20: <span id="change_20"> $0.00 </span></p>
                
            <h3 style="display:inline; color:white; font-size: 1.5em"> Total Cost: <span id="total_cost">$0.00</span> </h3>
            <button class="submit_button"> Place Order </button>
            <div id="transaction_list">
           		<p> Recent Transactions: </p>
           </div>
            
           <!-- 
           		<div style="text-align: center; margin-right:auto; margin-left:auto; padding-top: 15px;">
            	<label for="name"> Customers Name: </label>
        		<input type="text" name="name" id="xautoname" placeholder="Required!" />
       			<input type="submit" name="submit" onclick="javascript:$('form').submit();" />
                </div> 
           -->
        
          </div>
          <div class="content" style="position:relative; left:25%; height: 2000px;">
            <h1>Instructions</h1>
            <p>In the table below select all the items the customer has ordered, and the customer's name, and then click enter. </p>
            <p> The customer's name can be quick-selected by typing the first few letters of the name and <strong> pressing tab. </strong> </p>
            <p> When inventory runs out on an item, <strong>don't create a new item!</strong> Click the item's name and change its inventory to an arbitrarily high number.</p>
            
            <h5 id="customer_name"> </h5>
            <h5 class="alert" id="alert_response_field"> </h5>
            <?php

            $modify_url = 'http://cafe.tahabi.com/modify/item/';
		
		$out = "";
		
		foreach ($groups as $key => $val)
		{	
			$out .= "<tbody class=\"grouptitle\"> <tr> <td> $key </td> </tr> </tbody>\n";
			
			asort($val /*, SORT_STRING */); 
			
			foreach ($val as $key2 => $val2)
			{	
				if ($val2['disabled'] == '1')
				{ 
					continue; 	// Could be commented if desired to display disabled / no inventory items - currently (02/06/2013) only hiding disabled items
				}
				
				$out .= <<< CLOSE
				
<tr>
	<td> 
		<input type="checkbox" class="box" data-cost="{$val2['price']}" data-id="{$val2['id']}" />
		<label for="{$val2['id']}"><a href="{$modify_url}{$val2['id']}">{$val2['name']}</a></label> 
	</td> 
	<td class="disabled" colspan="2"> 
		<label for="{$val2['id']}[1]"> Quantity: </label> 
		<input type="number" class="quantity" maxlength="2" value='1' /> 
	</td>
</tr>

CLOSE;
			}
		}
		
		print <<< END
		
<table id="item_input">
 	<tr>
		<td> <label for="name"> Customers Name: </label> </td>
		<td> <input type="text" id="autoname" placeholder="Required!" /> </td> 
		<td> <button class="submit_button"> Place Order </button> </td>
	</tr>
	{$out}
 </table> 
		
END;

?>
           
            <!-- end .content -->
            </div>
          <div class="footer">
            <p>No Left Turn Here Cafe, Abizer Lokhandwala &copy; 2012 - 2013</p>
            <p> Production MVC edition </p>
            
            <!-- end .footer --></div>
          <!-- end .container --></div>
    </body>
</html>