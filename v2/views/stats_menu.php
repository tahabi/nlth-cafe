<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Stats View Menu</title>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
<link type="text/css" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/smoothness/jquery-ui.css"  rel="stylesheet" />
<script src="http://cafe.tahabi.com/functionality.js" type="text/javascript"></script>
</head>
<body>
    <?php include('views/navigation.html'); ?>
    <p> Please choose an option below: </p>
    <ol>
        <li> <a href="today"> All transactions today </a> </li>
        <li> <a href="yesterday"> All transactions yesterday </a> </li>
        <li> <a href="all"> All Sales Ever </a> </li>
        <li> <a href="overview"> A general overview </a> </li>
        <li> <a href="#"> Transactions from a specific date:  </a>
            <input type="text" name="date" id="date" />
            <input type="submit" onclick="redirect_stat_dates();" />
         </li>
        <li> <a href="sql"> Perform direct query against the database. Please do not use unless you know what you are doing. </a> </li>
    </ol>
</body>
</html>