  <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
 <html xmlns="http://www.w3.org/1999/xhtml"><head>
      <title>Button View</title>
      
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
    <link rel="stylesheet" href="http://code.jquery.com/mobile/1.3.0/jquery.mobile-1.3.0.min.css" />
      
	<script src="http://code.jquery.com/jquery-1.9.1.js"> </script>
    <script src="http://code.jquery.com/ui/1.10.2/jquery-ui..js"> </script>
    <script src="http://code.jquery.com/mobile/1.3.0/jquery.mobile-1.3.0.min.js"> </script>
    <script src="../js/functionality.js" type="text/javascript"> </script>
    
    
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
    <link rel="stylesheet" href="http://code.jquery.com/mobile/1.3.0/jquery.mobile-1.3.0.min.css" />
    
     <!-- <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script> -->
      <style type="text/css">
          
		   legend
		   {
			   font-size:larger;
			   text-align:center;
		   }
		   
      </style>

      <script type="text/javascript">

		   
		 /* $(function() {
			   $('label').on('click', function() {
				   var i = $(this).children('input');
				   i.prop('checked', !i.prop('checked'));
				   $(this).toggleClass('selected');
			   }); 
			   
		   }); */
		   
		   $(function() { 
		  	$('.item').button();
			 
			var cost = 0;
			
		   $('#cost').text(cost.toString()); 
		   
		   $('.item').change( function()
			   {
				  if ( $(this).attr('checked') )
				  {
					cost = cost + $(this).attr('data-cost'); 
				  }
				 $('#cost').text(cost.toString()); 
			   });
			 
			 
		   });
		   
		   
		   
      </script>
      

 </head>
 <body>
 <div data-role="page">
 
 <?php
 
 	$out = ''; 
 
 foreach ($groups as $key => $val)
		{	
			
			
			$out .= <<< END
			
			<div data-role="fieldcontain">
				<fieldset data-role="controlgroup" data-type='horizontal' data-corners='false' />
					<legend> {$key} </legend>			
END;
			
			asort($val, SORT_STRING); 
			
			foreach ($val as $key2 => $val2) // groupname => item array
			{ 	
				$inv = ($val2['inv'] <= 1) ? "<strong class=\"lowinv\"> {$val2['inv']} </strong>" : $val2['inv'];
				
				if ($val2['disabled'] == "yes")
				{ 
					continue; 	// Could be commented if desired to display disabled / no inventory items - currently (02/06/2013) only hiding disabled items
				}
				
				$out .= <<< CLOSE
						<input type="checkbox" name="selected_items[]" value="{$val2['id']}" data-cost="{$val2['cost']}" class="item" id="{$val2['id']}" />
						<label for="{$val2['id']}">
							{$val2['name']}
						</label>
CLOSE;
			}
			
			$out .= <<< END
			 </fieldset>
			</div>
END;
		}
		
		/* print <<< END
		
			<table id="item_input">
			 	<form action="http://cafe.radicaloutlook.com/mvc/controller.php?action=testing&q=button_view" method="POST" id="mainform" name="mform">
			 	<thead>
					<tr>
						<th> <label for="name"> Customer's Name: </label> </th>
						<th> <input type="text" name="name" id="autoname" placeholder="Required!" /> </th> 
						<th> <input type="submit" name="bvsubmit" /> </th>
					</tr>
				</thead>
				<tbody>
				
				
				
				{$out}
				
				</tbody>
			 	</form>
			 </table> 
		
END; */

// wrap it in a form and submit
print <<< END

<p> Cost so far: <span id="cost"> </span> </p>
<form action="http://cafe.radicaloutlook.com/mvc/testing/button_view" method="post">
<input type="text" name="name" placeholder="Name" id="autoname" />
<input type="submit" name="bvsubmit" value="submit" />
{$out}
</form>

END;


print $out . "</form>";

?>
</div>
 </body>
 </html>