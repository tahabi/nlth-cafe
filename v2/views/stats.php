<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Radical Outlook Statistics</title>
<style>
	thead th {font-weight:bolder;border-bottom: 2px solid #6678B1;color:#6678B1;font-family:"Trebuchet MS", Arial, Helvetica, sans-serif;}
	td {border-bottom: 1px solid #CCC;padding:10px 0px;}
	tbody > tr:hover {color: #063299;background-color: #F4F4F4;}
	table, p, form {margin-right:auto;margin-left:auto;/* display:block; */text-align:center;	}
	table {width:100%;border-collapse:collapse;}
	blockquote {font-size:24px;text-align:center;text-decoration:underline;color:#6678B1;font-family:"Trebuchet MS", Arial, Helvetica, sans-serif;}
</style>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
<link type="text/css" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/smoothness/jquery-ui.css"  rel="stylesheet" />
<script src="http://cafe.tahabi.com/functionality.js" type="text/javascript"></script>
</head>
<body>
<?php 
include('views/navigation.html');
$revenue = 0;

	$out = ( isset( $error ) ) ? $error : "";
	
	if ( isset( $_REQUEST['query'] )  )
	{
		_load_view('sql_form', array( 'sql' => $_REQUEST['query'], 'message' => ( $result->num_rows == 0 ) ? "No rows returned." : "" ));
	}
	
	$out = "<table>";
	$out .= "<thead>\n<tr>";
	
	if ( $result->num_rows != 0 ) 
	{		
			foreach ($result->fetch_fields() as $field)
			{	
				$out .= "<th>" . ucfirst($field->name) . "</th>";
			}
			
			$out .= "</tr>\n</thead>"; 
	$out .= "<tbody>"; 
	while ($entry = $result->fetch_assoc())
	{
		if ( isset( $entry['id'] ) ) 
		{
			$deletebutton = "deletetransaction(".$entry['id'].", '" . $viewname . "')";
			$modifybutton = "modifytransaction(".$entry['id'].", '" . $viewname . "')";
			$item_name = (isset($entry['iid'])) ? $entry['iid'] : $entry['id']; // redundant code to quickly fix a bug
		}
		
		if ( isset( $entry['iid'] ) ) 
		{
			$revenue += $entry['price'];
			$item_name = ''; 
			if ( isset($entry['id']) && isset($entry['iid']) ) 
			{
				$item_name = $id_map[$entry['iid']]['name'];
			}
			else
			{
				$item_name = (isset($entry['iid'])) ? $entry['iid'] : $entry['id'];
			}
		}

		if ( isset( $entry['price'] ))
		{
			if ( (float) $entry['price'] < 1.0 )
				$entry['price'] =  ( (float)$entry['price'] * 100 ) . "&cent";
			else
				$entry['price'] = '$'.$entry['price']; 
		}

		$out .= "<tr>";
		
		foreach ($entry as $key => $val)
		{
			$v = (empty($val) && $val != '0') ? '-' : $val; // 20130907 - I think this accounts for missing values in the transaction history, like missing names or deleted ids
			$i = (isset($item_name)) ? $item_name : ''; //(empty($item_name)) ? ( isset($entry['id']) ? $entry['id'] : '' ) : $item_name; // if the item has been previously deleted, put a question for its name
			// however, based on $item_names definition above, this will never be empty, so it will always default to printing $item_name
			$out .= "<td>" . ( ($key == 'id') ? $i : $v ). "</td>"; // {$key} => field name
		}
		
		if ( isset( $entry['iid'] ) )
		{
			$out .= <<< END
			<td> <button onClick="{$modifybutton}">Modify transaction</button> </td>
				<td> <button onClick="{$deletebutton}">Delete transaction</button> </td>
END;
		} // if this is showing transactions, then show the modify and delete buttons
		
		$out .= "</tr>";
	}   
	
	$out .= "</tbody>\n</table>";
	
	}
	
	print <<< END
	
	<blockquote> Revenue: \${$revenue} </blockquote>
	
	{$out}
	
END;

?>
</body>
</html>
