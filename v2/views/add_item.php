<html>
	<head>
    	<title>Add an item</title>
    </head>
    <style>
        table {margin-right: auto;margin-left: auto;text-align: center;}
        td {border-bottom: 1px solid #6678B1; padding:10px;}
        input {width: 200px;}
    </style>
<body>
    <?php include('views/navigation.html'); ?>

    <?php 
        if ( isset($add_success) && $add_success ) 
            echo "<h2> Item successfully added. </h2>";
    ?>
<table style="margin-right:auto; margin-left:auto; text-align:center;">
	<form method="post" action="http://cafe.tahabi.com/add/item">
	<tr>
    	<td>Group</td>
        <td> <input type="text" name="group" /> </td>
    </tr>
	<tr>
		<td>Name</td>
        <td> <input type="text" name="name" /> </td>
    </tr>
     <tr>
    	<td>ID Seed - Generally the initials of the group+name</td>
        <td> <input type="text" name="id" /> </td>
    </tr>
    <tr>
    	<td>Price to customers</td>
        <td> <input type="text" name="price" /> </td>
    </tr>
    <tr>
    	<td>Cost to us</td>
        <td> <input type="text" name="cost" /> </td>
    </tr>
    <tr>
        <td>Attributes</td>
        <td> <input type="text" name="attr" /> </td>
    </tr>
    <tr>
    	<td>Insert as Disabled Item?</td>
        <td> <input type="checkbox" name="disabled" value="1" /> </td>
    </tr>
    <tr> 
    	<td>Click to add item</td>
        <td> <input type="submit" name="submit" /> </td>
    </tr>
    </form>
</table>
</body>
</html>