<?php
$out = "";
		
		foreach ($groups as $key => $val)
		{	
			$out .= "<div class=\"item_container\">";

			$out .= "<h1 class=\"grouptitle\"> $key </h1>\n";

			$out .= "<ul class=\"item_list\">";
			
			asort($val /*, SORT_STRING */); 
			
			foreach ($val as $key2 => $val2)
			{
				
				$out .= <<< CLOSE
				
				<li>
					<p onclick="newfollow('{$val2['id']}')">{$val2['name']}</p>
				</li>

CLOSE;
			}

			$out .= "</ul></div>";
		}
?>
<html>
<head> 
<title> Modify Items </title>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
<script src="http://cafe.tahabi.com/functionality.js"> </script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/masonry/3.1.1/masonry.pkgd.js"> </script>
<style>
._pending {
	background-color: gold !important;
}
._success {
	background-color: greenyellow !important;
}
._normal {
	background-color: lavender;
	padding: 10px;
	border: 1px solid lightcoral;
	text-align: center;
}
.grouptitle { 
	font-size: 36px; 
	color: red;
}
.item_list { 
	display: block; 
	list-style-type: none; 
	line-height: 10px; 
	text-decoration: underline;
}
.item_box { 
	width: 1000px;
}
.change_form { 
	display: inline; 
	top: 100px; 
	left: 1050px; 
	position: fixed; 
}
.p { 
	text-decoration: underline; 
}
.item_container { 
	/*border: 1px solid black;*/
	width: 230px;
}
ul { 
	list-style-type: none;
	padding-left: 3px;
}
input[type=text]
{
	padding:5px;
	width: 200px;
}
td { 
	padding: 5px; 
}
</style>
<body>
	<?php include('views/navigation.html'); ?>
	<p> Click on one of the following items to edit it. </p>
<div class="item_box js-masonry" data-masonry-options='{ "columnWidth": 240, "itemSelector": ".item_container", "gutter": 0 }'>
<?php print $out; ?>
</div>
			
<div class="change_form">
	<h1> Editing item: <span class="item_name"></span> (<span class="item_id"></span>)  </h1>
<table>
	<!-- <form method="post" id="update_form" onsubmit="item_change_submit();" action="http://cafe.tahabi.com/item/modify"> -->
	<tr>
    	<td>Group</td>
        <td> <input type="text" name="item_group" class="item_group" value="" /> </td>
    </tr>
	<tr>
		<td>Name</td>
        <td> <input type="text" name="item_name" class="item_name" value="" /> </td>
    </tr>
    <tr>
    	<td>ID Seed (initials of the group + name)</td>
        <td> <input type="text" name="item_id" class="item_id" value="" /> </td>
    </tr>
    <tr>
    	<td>Price</td>
        <td> <input type="text" name="item_price" class="item_price" value="" /> </td>
    </tr>
    <tr>
    	<td>Cost to us</td>
        <td> <input type="text" name="item_cost" class="item_cost" value="" /> </td>
    </tr>
    <tr>
        <td>Attributes</td>
        <td> <input type="text" name="item_attr" class="item_attr" value="" /> </td>
    <tr>
    	<td>Disabled Item?</td>
        <td> Yes: <input type="radio" name="disabled" class="item_disabled_true" value='1' /> No: <input type="radio" name="disabled" class="item_disabled_false" value='0' /></td>
    </tr>
    <tr>
    	<td>Save</td>
        <td> <button onclick="item_change_submit()" value="Submit">Save</button> </td>
    </tr>
    <tr>
    	<td colspan="2">
    		<p class="_normal"> Please click on the name of the item you want to edit, <br /> make your changes above and click "submit" to save </p>
    	</td>
    </form>
</table>
<div class="result_data">
</div>
</div>

</body>
</html>