<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
   		<title>No Left Turn Here Cafe Online Remote Order Form</title>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			<link href="http://cafe.radicaloutlook.com/style.css" rel="stylesheet" type="text/css" />
			<script src="http://cafe.radicaloutlook.com/js/jquery/js/jquery-1.7.2.min.js"> </script>
			<script src="http://cafe.radicaloutlook.com/js/jquery/js/jquery-ui-1.8.23.custom.min.js"> </script>
			<link type="text/css" href="http://cafe.radicaloutlook.com/js/jquery/css/smoothness/jquery-ui-1.8.23.custom.css"  rel="stylesheet" />
			<link type="text/css" href="http://cafe.radicaloutlook.com/form.css" rel="stylesheet" />
			<script src="http://cafe.radicaloutlook.com/js/custom-form-elements.js"></script>
			<script src="http://cafe.tahabi.com/functionality.js" type="text/javascript"></script>
	</head>

	<body>
    	<div class="container">
          <div class="header"><a href="http://cafe.radicaloutlook.com/mvc/"><img src="http://cafe.radicaloutlook.com/logo.png" alt="NLTH" width="300" height="120" id="Insert_logo" /></a> 
            <!-- end .header -->
          </div>
          <div class="sidebar1" style="position:fixed;">
            
              
                <p> Change from a 5: <span id="change_5"> $0.00 </span></p>
                <p> Change from a 10: <span id="change_10"> $0.00 </span></p>
                <p> Change from a 20: <span id="change_20"> $0.00 </span></p>
                
            <h3 style="display:inline; color:white; font-size: 1.5em"> Total Cost: $<span id="total_cost"></span> </h3>
            <button onclick="document.mform.submit.click();" id="second"> Submit Items </button>
            
           <!-- 
           		<div style="text-align: center; margin-right:auto; margin-left:auto; padding-top: 15px;">
            	<label for="name"> Customers Name: </label>
        		<input type="text" name="name" id="xautoname" placeholder="Required!" />
       			<input type="submit" name="submit" onclick="javascript:$('form').submit();" />
                </div> 
           -->
        
          </div>
          <div class="content" style="position:relative; left:25%;">
            <h1>Instructions</h1>
            <p> Please select the items you wish to order below and submit. </p>
            <p> Afterwords, please come with your money to pick up your items - certain items, such as those requiring cooking, will be delivered to you. </p>
            <p> Please note that abuse of this service will lead to the loss of cafe priveleges <em> permanently. </em> </p>
            
            <p> <strong> <?php if (isset($custname) || $custname != "") { echo $custname . "'s order has been processed."; } ?></strong> </p>
            <p> <strong> <?php if (isset($message)) { echo $message; } ?></strong> </p>
            <?php
		
		$out = "";
		
		foreach ($groups as $key => $val)
		{	
			$out .= "<tbody class=\"grouptitle\"> <tr> <td> $key </td> </tr> </tbody>\n";
			
			asort($val, SORT_STRING); 
			
			foreach ($val as $key2 => $val2)
			{ 	
			
				$inv = ($val2['inv'] <= 1) ? "<strong class=\"lowinv\"> {$val2['inv']} </strong>" : $val2['inv'];
			//	$disabled = ($inv <= 0) ? "disabled=\"disabled\"" : "";
			//	$available = (empty($disabled)) ? "" : "<span style=\"color:red; font-size:24px;\">Not Available! </span>";
				
				if ($val2['disabled'] == "yes") //$val2['inv'] <= 0 || 
				{ 
					continue; 	// Could be commented if desired to display disabled / no inventory items - currently (02/06/2013) only hiding disabled items
				}
				
				$out .= <<< CLOSE
				
				<tr>
					<td> 
						<input type="checkbox" name="{$val2['id']}[0]" value="true" data-cost="{$val2['cost']}" class="item" />
						<label for="{$val2['id']}[0]">{$val2['name']}</label> 
					</td> 
					<td> 
						<label for="{$val2['id']}[1]" style="display:none;"> Quantity: </label> 
						<input type="number" name="{$val2['id']}[1]" max="{$val2['inv']}" maxlength="2" disabled="disabled" style="display:none;" /> Inventory: <span class="inv">{$inv}</span> 
					</td>
				</tr>
CLOSE;
			}
		}
		
		print <<< END
		
			<table id="item_input" onclick="update_total(this);">
			 	<form action="http://cafe.radicaloutlook.com/mvc/controller.php?action=testing&q=order_online" method="post" onsubmit="return checkname();" id="mainform" name="mform">
			 	<tr>
						<td> <label for="name"> Your Name: </label> </td>
						<td> <input type="text" name="name" id="autoname" placeholder="Required!" /> </td> 
						<td> <input type="submit" name="submit" /> </td>
				</tr>
				
				{$out}
					
			 	</form>
			 </table> 
		
END;

?>
            <div>
           </div>
            <!-- end .content --></div>
          <div class="footer">
            <p>No Left Turn Here Cafe, Abizer Lokhandwala &copy; 2012 - 2013</p>
            <p> <strong> Prototype </strong> MVC edition </p>
            <p> Please report all bugs, problems, and errors here: <a href="mailto:abizerkl@gmail.com">abizerkl@gmail.com</a></p>
            
            <!-- end .footer --></div>
          <!-- end .container --></div>
    </body>
</html>